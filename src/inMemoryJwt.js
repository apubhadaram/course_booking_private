import { getUserDetails } from "./loggedIn.js"

export default (() => {
    const logoutEvent = 'ra-logout'
    const refreshEndpoint = `http://localhost:3000/api/users/refresh-token`
    let refreshTimeOutId = false;
    let inMemoryJWT = null;

    window.addEventListener('storage', (event) => {
        if (event.key === logoutEvent) {
            inMemoryJWT = null;
        }
    });

    // This countdown feature is used to renew the JWT in a way that is transparent to the user before it's no longer valid.
    const refreshToken = (delay) => {
        refreshTimeOutId = window.setTimeout(
            getRefreshedToken,
            delay * 1000 - 5000
        ); // Validity period of the token in seconds, minus 5 seconds
    };

    // const setRefreshTokenEndpoint = endpoint => refreshEndpoint = endpoint;

    // For reloading
    const checkAuth = async () => {
        console.log('checkAuth');
        if (!getToken()) {
            return await getRefreshedToken()
        } else {
            return false
        }
    }


    // The method makes a call to the refresh-token endpoint
    // If there is a valid cookie, the endpoint will return a fresh jwt.
    const getRefreshedToken = async () => {
        const user = getUserDetails(getToken())
        const response = await fetch(refreshEndpoint, {
            method: 'POST',
            headers: new Headers({ 'Content-Type': 'application/json' }),
            body: JSON.stringify(user)
        })
        let token, tokenExpiry;
        if (response.status !== 200) {
            eraseToken();
            global.console.log(
                'Failed to renew the jwt from the refresh token.'
            );
            return { token: null };
        } else {
            ({ token, tokenExpiry } = await response.json());
        }

        if (token) {
            setToken(token, tokenExpiry);
            return true;
        }

        return false;
    };

    const abortRefreshToken = () => {
        if (refreshTimeOutId) {
            window.clearTimeout(refreshTimeOutId);
        }
    };

    const getToken = () => inMemoryJWT;

    const setToken = (token, delay) => {
        inMemoryJWT = `Bearer ${token}`;
        abortRefreshToken(refreshTimeOutId)
        refreshToken(delay)
        return true;
    };

    const eraseToken = () => {
        inMemoryJWT = null;
        abortRefreshToken()
        window.localStorage.setItem(logoutEventName, Date.now());
        return true;
    }

    const setLogoutEventName = name => logoutEventName = name;

    return {
        eraseToken,
        getToken,
        setLogoutEventName,
        // setRefreshToken,
        checkAuth,
        setToken,
    }
})()