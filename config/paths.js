import path from 'path';

export default {
  src: path.resolve('./', '../Client/src'),
  build: path.resolve('./', '../Client/build'),
  public: path.resolve('./', '../public'),
};
