import {configureNav, adjustNavColor} from './nav.js';
import $ from 'jquery';

//
const RATIO = 0.1;

let options = {
  root: null,
  rootMargin: '0px',
  threshold: RATIO,
};

const configureDown = () => {
  $(`#down`).on('click', () => {
    document.documentElement.scrollTo(
      0,
      document.documentElement.clientHeight * 1.06
    );
  });
};

const handler = (entries, observer) => {
  entries.forEach(entry => {
    if (entry.intersectionRatio > RATIO) {
      entry.target.classList.add(`show`);
      entry.target.classList.remove(`hidden`);
      observer.unobserve(entry.target);
    }
  });
};

const size = {
  x: innerWidth,
};

// const hello = HTML.query(`#hello`)
// hello.classList.add(`hidden`)

let observer = new IntersectionObserver(handler, options);
// observer.observe(hello)
//

export const renderVerifiedHome = async token => {
  adjustNavColor();
  configureDown();
  configureNav(token);

  fixVideo();
};

let left = 0;
let ml = 0;

const fixVideo = () => {
  left -= (size.x - innerWidth) / 2;
  ml =
    (+$(`video`).css(`width`).replace('px', '') -
      +$(`.landing-page`).css('width').replace('px', '')) /
    2;

  $(`.video-container`).css({left, 'margin-left': -ml});
  size.x = innerWidth;
};

export const renderHome = () => {
  adjustNavColor();
  configureDown();
  window.addEventListener(`resize`, fixVideo);
  configureNav();

  fixVideo();
};
