import HtmlWebpackPlugin from 'html-webpack-plugin';
import webpack from 'webpack';
import {merge} from 'webpack-merge';
import common from './webpack.common.js';
import paths from './paths.js';

// Not sure the function from here to...
// import {createRequire} from 'module';

// const require = createRequire(import.meta.url);
// ...here. I have an idea though.

export default merge(common, {
  mode: 'development',

  output: {
    path: paths.build,
    filename: `./assets/js/[name].bundle.js`,
  },

  //
  devtool: 'eval-source-map',

  // Spin up server
  devServer: {
    contentBase: paths.build,
    historyApiFallback: true,
    compress: true,
    hot: true,
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node-modules/,
        use: ['babel-loader'],
      },
      {
        test: /\.scss$/,
        use: ['style-loader', 'css-loader', 'postcss-loader', 'sass-loader'],
      },
      {
        test: /\.(woff(2)?|eot|ttf|otf|)$/i,
        type: 'asset/inline',
      },
      {
        test: /\.(png|svg|jpg|jpeg|gif)$/i,
        type: 'asset/resource',
      },
    ],
  },

  plugins: [
    // Only Updates Changes
    new webpack.HotModuleReplacementPlugin(),

    new HtmlWebpackPlugin({
      filename: `index.html`,
      template: `./pages/index.html`,
      minify: false,
    }),
    new HtmlWebpackPlugin({
      filename: `courses/addCourse/index.html`,
      template: `./pages/addCourse.html`,
      minify: false,
    }),
    new HtmlWebpackPlugin({
      filename: `courses/editCourse/index.html`,
      template: `./pages/editCourse.html`,
      minify: false,
    }),
    new HtmlWebpackPlugin({
      filename: `courses/index.html`,
      template: `./pages/courses.html`,
      minify: false,
    }),
    new HtmlWebpackPlugin({
      filename: `course/index.html`,
      template: `./pages/course.html`,
      minify: false,
    }),
    new HtmlWebpackPlugin({
      filename: `account/index.html`,
      template: `./pages/account.html`,
      minify: false,
    }),
  ],

  resolve: {
    fallback: {
      // In case crypto doesn't work
      // crypto: require.resolve('crypto-browserify'),
      crypto: false,
    },
  },
});
