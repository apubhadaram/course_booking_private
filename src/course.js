import {getUserDetails} from './loggedIn.js';
import {configureNav} from './nav.js';
import Swal from 'sweetalert2';
import $ from 'jquery';
import {repeat} from 'lodash';
import imgs from './instructorImgs.js';
import {API} from './helper.js';

export const renderAddCourse = token => {
  configureNav(token);
  const courseForm = document.querySelector(`#createCourse`);
  courseForm.addEventListener(`submit`, e => {
    e.preventDefault();

    const inputs = courseForm.getElementsByTagName(`input`);
    const description = courseForm.getElementsByTagName(`textarea`);
    const labels = [`name`, `price`, `image`, `instructors`];

    const course = {};

    try {
      labels.forEach((label, i) => {
        if (!inputs[i].value) throw {message: `Please input course ${label}`};
        course[label] = inputs[i].value;
      });

      course[`price`] = +course[`price`];
      course[`description`] = description[0].value;

      createCourse(course, token);
    } catch (err) {
      console.log(err);
      Swal.fire({
        icon: `error`,
        html: `<p style="color: #fff">${err.message}</p>`,
        background: '#364f6b',
      });
    }
  });
};

const createCourse = async (course, token) => {
  try {
    const res = await fetch(`${API}/instructors/?name=${course.instructors}`);
    const {_id: instructorId} = await res.json();
    course.instructors = [{instructorId}];
    let response = await fetch(`${API}/courses/create`, {
      method: `POST`,
      headers: {
        'Content-Type': `application/json`,
        authorization: token,
      },
      body: JSON.stringify(course),
    });
    if (response.status === 201) {
      Swal.fire({
        icon: `success`,
        title: `Great!`,
        text: `Course Creation Successful`,
      });
    } else if (response.status === 403) {
      throw {
        title: `Forbidden`,
        text: `You must be an admin to create a course.`,
      };
    } else if (response.status >= 400) {
      throw {
        title: `Course Creation Failed`,
        text: `Course is already in the database.`,
      };
    }
  } catch (err) {
    console.log(err);
    console.log(err.title, err.text);
    Swal.fire({
      icon: `error`,
      title: err.title,
      text: err.text,
      background: '#041d47a1',
      backdrop: 'rgb(0, 0, 0, 0.4)',
    });
  }
};

export const renderVerifiedCourses = async token => {
  try {
    // Configuring nav
    configureNav(token);

    const courses = await getCourses();
    await displayCourses(courses, token);
  } catch (err) {
    console.error(err);
  }
};

export const renderCourses = () => {
  try {
    // Configuring Navbar
    configureNav();

    getCourses().then(courses => displayCourses(courses));
  } catch (err) {
    console.error(err);
  }
};

export const renderCourse = async token => {
  try {
    token ? configureNav(token) : configureNav();
    displayCourse(token);
  } catch (err) {
    console.error(err);
  }
};

const getCourse = async id => {
  try {
    const response = await fetch(`${API}/courses/?id=${id}`);
    const course = await response.json();
    return course;
  } catch (err) {
    console.error(err);
  }
};

const getCourses = async () => {
  try {
    const res = await fetch(`${API}/courses`);
    const jsonRes = await res.json();
    return jsonRes.data;
  } catch (err) {
    console.error(err);
  }
};

const displayCourse = async (token = false) => {
  try {
    // Getting Course Details
    const query = new URLSearchParams(location.search);

    if (!query) {
      location.assign('../');
      return;
    }
    const id = query.get('id');
    const course = await getCourse(id);

    if (course.image) {
      $(`#courseHeader`).css({
        background: `url(${course.image}) no-repeat center`,
        'background-size': '100%',
      });
    }

    // Course Name
    $(`#courseName`).append(course.name);
    // Course Details
    $(`#courseDescription`).append(repeat('&nbsp;', 4) + course.description);
    // Course Card Details
    $(`#courseCard`).append(
      `<div>
      <div class="cardDetails">
        <div class="icon">₱</div>
        <div class="texts">
          <p>${course.price}</p>
          <p class="sub">Financial Aid Available</p>
        </div>
      </div>
      <div class="cardDetails">
        <div class="icon">
          <i class="far fa-clock"></i>
        </div>
        <div class="texts">
          <p>${course.duration}</p>
          <p class="sub">2-3 hours per week</p>
        </div>
      </div>
    </div>
    <button id="enroll" type="button"> ENROLL </button>`
    );
    // Course Instructors
    if (course.instructors) {
      for (let instructor of course.instructors) {
        const res = await fetch(
          `${API}/instructors/?id=${instructor.instructorId}`
        );
        // console.log(await res.text());
        instructor = await res.json();
        let img;
        switch (instructor.name) {
          case 'Dr. Ronald White':
            img = imgs.white;
            break;
          case 'Dr. Arijit Bingham':
            img = imgs.bingham;
            break;
          case 'Dr. Manilow Mandy':
            img = imgs.manilow;
            break;
          case 'Dr. Sara Brooks':
            img = imgs.brooks;
            break;
          case 'Dr. Wei Ying':
            img = imgs.ying;
            break;
        }
        $(`#instructors`).append(`<div class="instructor">
                <img class="instructorImg" src=${img} alt="${instructor.name}">
                <div class="texts">
                  <h3>${instructor.name}</h3>
                  <h4>Professor</h4>
                  <p>${instructor.profession}</p>
                </div>
              </div>`);
      }
    }

    if (token) {
      const user = await getUserDetails(token);
      let enrolled = false;
      for (let enrollee in course.enrollees) {
        console.log(enrollee);
        if (enrollee.userId == user.id) {
          enrolled = true;
        }
      }

      if (enrolled) {
        $(`#enroll`).text('ENROLLED').css({
          background: '#747474',
          border: '2px solid #383838 !important',
          cursor: 'default',
        });
      } else {
        $(`#enroll`).on('click', enroll);
      }
    } else {
      $(`#enroll`).on('click', () => (location.hash = 'login'));
    }
  } catch (err) {
    console.error(err);
  }
};

const displayCourses = async (courses, token = false) => {
  try {
    const user = token ? await getUserDetails(token) : false;

    const isAdmin = user.isAdmin;

    if (isAdmin) {
      $(`#courses-container`).prepend(
        `<div class="btn btn-warning add-btn"><h3>Add Course</h3></div>`
      );
      $(`#courses-container`).append(
        `<div class="btn btn-warning add-btn"><h3>Add Course</h3></div>`
      );
      for (let {_id, name, description, price, isActive, image} of courses) {
        if (isActive) {
          $(`#active-courses`).append(`
            <div id="${name.replace(' ', '')}" 
            class="course-card">
                <div id="cardImg${_id}" class="card-img-top" alt="Card image"></div>
                <div class="card-body">
                    <div class="course-texts">
                        <h3 class="card-title">${name}</h5>
                        <p class="card-text">${
                          description.length < 200
                            ? description
                            : description.slice(0, 200) + '...'
                        }</p>
                        <h4>₱${price}</h3>
                    </div>
                    <div class="buttons">
                        <a href="../course/?id=${_id}" class="btn btn-primary">Details</a>
                        <a href="../editCourse/?id=${_id}" class="btn btn-warning">Edit</a>
                        <a id="archive${_id}" class="btn btn-danger">Archive</a>
                    </div>
                </div>
            </div>`);

          $(`#archive${_id}`).on('click', () => deleteCourse(_id, token));
        } else {
          $(`#archived-courses`).append(`
            <div id="${name.replace(' ', '')}" 
            class="course-card">
                <div id="cardImg${_id}" class="card-img-top" alt="Card image"></div>
                <div class="card-body">
                    <div class="course-texts">
                        <h5 class="card-title">${name}</h5>
                        <p class="card-text">${
                          description.length < 50
                            ? description
                            : description.slice(0, 50) + '...'
                        }</p>
                        <h3>${price}</h3>
                    </div>
                    <div class="buttons">
                        <a href="../course/?id=${_id}" class="btn btn-primary">Details</a>
                        <a href="../editCourse/?id=${_id}" class="btn btn-warning">Edit</a>
                        <a id="restore${_id}" class="btn btn-success">Restore</a>
                    </div>
                </div>
            </div>`);

          $(`#restore${_id}`).on('click', () => restoreCourse(_id, token));
        }

        $(`#cardImg${_id}`).css({
          background: `url(${image}) no-repeat center`,
          'background-size': '100%',
        });

        // $(`#${name.replace(' ', '')}`).css({});
      }

      document.querySelectorAll('.add-btn').forEach(e => {
        e.addEventListener('click', () => {
          location.assign('courses/addCourse');
        });
      });
    } else {
      for (let {_id, name, description, price, isActive, image} of courses) {
        if (isActive) {
          $(`#active-courses`).append(`
            <div id="${name.replace(' ', '')}" 
            class="course-card">
                <div id="cardImg${_id}" class="card-img-top" alt="Card image"></div>
                <div class="card-body">
                    <div class="course-texts">
                        <h3 class="card-title">${name}</h5>
                        <p class="card-text">${
                          description.length < 200
                            ? description
                            : description.slice(0, 200) + '...'
                        }</p>
                        <h4>₱${price}</h3>
                    </div>
                    <div class="buttons">
                        <a href="../course/?id=${_id}" class="btn btn-primary">Details</a>
                    </div>
                </div>
            </div>`);

          $(`#cardImg${_id}`).css({
            background: `url(${image}) no-repeat center`,
            'background-size': '100%',
          });
        }
      }

      document.querySelectorAll('.add-btn').forEach(e => {
        e.remove();
      });
    }

    $(`#coursesSpinner`).remove();
  } catch (err) {
    console.error(err);
  }
};

const deleteCourse = (id, token) => {
  try {
    fetch(`${API}/courses/`, {
      method: 'DELETE',
      headers: {
        'Content-Type': `application/json`,
        authorization: token,
      },
      body: JSON.stringify({id}),
    }).then(() => {
      location.reload();
    });
  } catch (err) {
    console.error(err);
  }
};

const restoreCourse = (id, token) => {
  try {
    fetch(`${API}/courses/${id}`, {
      method: 'PUT',
      headers: {
        authorization: token,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({isActive: true}),
    }).then(() => {
      location.reload();
    });
  } catch (err) {
    console.error(err);
  }
};

const enroll = async () => {
  try {
    const token = sessionStorage.getItem('jwt');
    const query = new URLSearchParams(location.search);
    const courseId = query.get('id');

    const res = await fetch(`${API}/users/enroll`, {
      method: 'PUT',
      headers: {
        authorization: token,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        courseId,
      }),
    });

    if (res.status < 400) {
      location.reload();
    }
  } catch (err) {
    console.error(err);
  }
};
