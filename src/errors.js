export class AuthenticationError extends Error {
    constructor(title, message) {
        super(message);
        this.title = title
        this.name = "AuthenticationError"
    }
}

// class InvalidPassword extends AuthenticationError {
//     constructor()
// }

// class EmailDoesNotExistError extends AuthenticationError {
//     constructor()
// }