import * as Swal from 'sweetalert2';
import {AuthenticationError} from './errors.js';

export const validateReg = e => {
  e.preventDefault();

  const regForm = e.target;

  const inputs = regForm.getElementsByTagName(`input`);
  const labels = [
    `firstName`,
    `lastName`,
    `email`,
    `mobileNo`,
    `password`,
    `verifyPassword`,
  ];

  const user = {};

  try {
    // Assigning input into user object and validate if any is empty.
    for (let i in labels) {
      if (!inputs[i].value)
        throw {
          title: `Unfilled Field`,
          text: `Please fill all the necessary fields`,
        };
      user[labels[i]] = inputs[i].value;
    }

    // Password: alphanumeric and 1 special char
    let reg = /^(?=.{8,15})(?=.*[\w])(?=.*[^\w]).*$/gi;
    if (reg.test(user.password)) {
      registerUser(user);
    } else {
      throw {
        title: `Invalid Password`,
        text: `Please make sure the your password has at least 1 special character and at least 8 characters up to 15 characters.`,
      };
    }
  } catch (err) {
    console.error(err);
    Swal.fire({
      icon: `error`,
      title: err.title,
      text: err.text,
    });
  }
};

const registerUser = async user => {
  // mobileNo should accept both Foreign and Local mobile Number formats.
  if (
    user[`password`] === user[`verifyPassword`] &&
    user[`mobileNo`].length === 11
  ) {
    try {
      const response = await fetch(
        `https://course-booking-server.herokuapp.com/api/users/register`,
        {
          method: `POST`,
          headers: {
            'Content-Type': `application/json`,
          },
          body: JSON.stringify(user),
        }
      );

      let data = await response.json();

      response.status < 300
        ? Swal.fire({
            icon: `success`,
            text: `Registered Successfully`,
          })
        : Swal.fire({
            icon: 'error',
            title: 'Email Already Taken',
            text: 'Please Try Again',
          });

      location.hash = '#login';
    } catch (err) {
      console.error(err);
      Swal.fire({
        icon: 'error',
        text: err.message,
      });
    }
  } else {
    Swal.fire({
      icon: `error`,
      title: `Invalid Password`,
      text: `Please make sure your password are the same.`,
    });
  }
};

export const authLog = e => {
  e.preventDefault();
  const logForm = document.querySelector(`#loginForm`);

  // Assigning all the inputs to the user object and verifies whether
  const inputs = logForm.getElementsByTagName(`input`);
  const labels = [`email`, `password`];
  const user = {};

  try {
    for (let i in labels) {
      if (!inputs[i].value)
        throw new AuthenticationError(
          `Unfilled Field`,
          `Please fill all the necessary fields`
        );
      user[labels[i]] = inputs[i].value;
    }

    loginUser(user);
    // location.reload();
  } catch (err) {
    if (err instanceof AuthenticationError) {
      Swal.fire({
        icon: `error`,
        title: err.title,
        text: err.text,
      });
    } else if (err instanceof SyntaxError) {
      console.error(err.message);
    } else {
      throw err;
    }
  }
};

const loginUser = async user => {
  try {
    // mem.setRefreshTokenEndpoint(`http://localhost:3000/api/users/refresh-token`)
    const response = await fetch(
      `https://course-booking-server.herokuapp.com/api/users/login`,
      {
        method: `POST`,
        headers: {
          'Content-Type': `application/json`,
        },
        body: JSON.stringify(user),
      }
    );

    // If response is valid
    if (response.status === 302) {
      // Getting and setting jwt token
      const {data: token} = await response.json();

      // mem.setToken(token, 60 * 5)

      sessionStorage.setItem(`jwt`, `Bearer ${token}`);

      // Redirection
      location.reload();

      return;
    } else {
      Swal.fire({
        icon: `error`,
        title: `Login Error`,
        text: `Please make sure you have the right credentials.`,
      });
    }
  } catch (err) {
    console.log(err);
    Swal.fire({
      icon: 'error',
      title: 'Error!',
      text: 'Something went wrong :(',
    });
  }
};
