import {CleanWebpackPlugin} from 'clean-webpack-plugin';

import paths from './paths.js';

import {createRequire} from 'module';
// const require = createRequire(import.meta.url);

export default {
  entry: {
    app: paths.src + '/index.js',
  },

  plugins: [new CleanWebpackPlugin()],

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node-modules/,
        use: ['babel-loader'],
      },

      // HTML
      {
        test: /\.html$/i,
        // exclude: [/node_modules/, require.resolve('./index.html')],
        loader: 'html-loader',
        options: {
          minimize: false,
        },
      },
    ],
  },

  resolve: {
    fallback: {
      // In case crypto doesn't work
      // crypto: require.resolve('crypto-browserify'),
      crypto: false,
    },
  },
};
