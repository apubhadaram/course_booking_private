import bingham from '../asset/instructors/ArijitBingham.jpg';
import manilow from '../asset/instructors/MandyManilow.jpg';
import white from '../asset/instructors/RonaldWhite.jpg';
import brooks from '../asset/instructors/SaraBrooks.jpg';
import ying from '../asset/instructors/WeiYing.jpg';

export default {
  bingham,
  manilow,
  white,
  brooks,
  ying,
};
