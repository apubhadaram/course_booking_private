import {validateReg} from './auth.js';
import {showForm, showReg, processPass} from './forms.js';
// import mem from "./inMemoryJwt.js"s

export default () => {
  showForm();
  showReg();
  processPass();
  const regForm = document.querySelector(`#registerUser`);
  regForm.addEventListener(`submit`, validateReg);
};
