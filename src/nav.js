import {getUserDetails} from './loggedIn.js';
import MD5 from 'crypto-js/md5.js';
import $ from 'jquery';
import {API} from './helper.js';
import {result} from 'lodash';
import course from '../../Server/models/course.js';

const shownResults = [];

export const configureNav = async token => {
  try {
    const nav = document.querySelector(`#zuitterNav`);
    const navList = nav.getElementsByTagName(`li`);

    const isLoggedIn = token ? await getUserDetails(token) : false;
    const isAdmin = isLoggedIn ? isLoggedIn.isAdmin : false;

    if (isAdmin) {
      let {email: userEmail} = await getUserDetails(token);
      const HASH = MD5(userEmail);

      // let imgRadius = nav.clientHeight
      let img = `https://www.gravatar.com/avatar/${HASH}?s=55&d=mp`;

      for (let li of navList) {
        if (li.innerText === `Sign Up` || li.innerText === `Login`) {
          li.innerText = '';
        } else if (li.id === 'profPic') {
          li.getElementsByTagName('img')[0].setAttribute('src', img);
        }
      }

      $(`#profile`).remove();
    } else if (isLoggedIn) {
      let {email: userEmail} = await getUserDetails(token);
      const HASH = MD5(userEmail);

      // let imgRadius = nav.clientHeight
      let img = `https://www.gravatar.com/avatar/${HASH}?s=55&d=mp`;

      for (let li of navList) {
        if (li.innerText === `Sign Up` || li.innerText === `Login`) {
          li.innerText = '';
        } else if (li.id === 'profPic') {
          li.getElementsByTagName('img')[0].setAttribute('src', img);
        }
      }
    } else {
      for (let li of navList) {
        if (li.innerText === `Logout`) {
          li.innerText = '';
        } else if (li.id === `profPic`) {
          li.remove();
        }
      }
    }

    $(`#search`).on('input', configSearchBar);

    $(`#logout`).on('click', () => {
      location.hash = `logout`;
    });
  } catch (err) {
    console.warn(err);
  }
};

const configSearchBar = async e => {
  if (e.target.value) {
    const results = $(`#results`).children();
    // console.log(`Result ID!!!`);
    const resultsIDs = (() => {
      let ids = [];
      results.each((i, result) => ids.push(result.id));
      return ids;
    })();

    let query = new URLSearchParams({name: e.target.value});
    try {
      const res = await fetch(`${API}/courses/?${query.toString()}`);
      const courses = await res.json();

      if (courses.length) {
        const courseIDs = courses.map(course => course._id);
        console.log(resultsIDs);
        console.log(courseIDs);

        console.log(results.length);
        for (let i = results.length - 1; i >= 0; i--) {
          // console.log(results[i]);
          console.log(!(results[i].id in courseIDs));
          if (!(results[i].id in courseIDs)) {
            $(`#${results[i].id}`).remove();
          }
        }

        courses.forEach(course => {
          if (!(course._id in resultsIDs)) displayResult(course);
        });
      }
    } catch (err) {}

    processEraser();
  } else {
    $(`#results`).html(``);
    $(`.fa-eraser`).addClass('hidden');
  }
};

const displayResult = course => {
  $(`#results`).append(`
              <div id="${course._id}" class="result" href="../course?id=${course._id}" >
                <div class="resultImg" id="img${course._id}"></div>
                <h4>${course.name}</h4>
              </div>`);
  $(`#${course._id}`).on('click', () => {
    location.assign(`/course?id=${course._id}`);
  });
  $(`#img${course._id}`).css({
    background: `url(${course.image}) no-repeat center`,
    'background-size': '100%',
  });
};

const processEraser = () => {
  $(`.fa-eraser`)
    .removeClass('hidden')
    .on('click', () => {
      $(`#search`).val('').trigger('focus');
      $(`#results`).html(``);
      $(`.fa-eraser`).addClass('hidden');
    });
};

export const adjustNavColor = () => {
  addEventListener(`scroll`, () => {
    const doc = document.documentElement;
    if (doc.scrollTop <= doc.clientHeight) {
      $(`#main-nav`).addClass('transparent');
    } else {
      $('#main-nav').removeClass('transparent');
    }
  });
};
