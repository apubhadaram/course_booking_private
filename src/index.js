import renderRegister from './register.js';
import renderLogin from './login.js';
import {renderHome, renderVerifiedHome} from './home.js';
import {
  renderCourses,
  renderCourse,
  renderVerifiedCourses,
  renderAddCourse,
} from './course.js';
import '@popperjs/core';
import {} from 'bootstrap';
import $ from 'jquery';
import './main.scss';
import {updateFormPos} from './forms.js';
import loader from './loader.js';
import {renderAccount} from './account.js';
// import dotenv from "dotenv"

// dotenv.config()
// import mem from "./inMemoryJwt.js"
const PORT = 8080;

// Loader
$(`body`).append(`
    <div class="loader-wrapper">
      <canvas id="canvas"></canvas>
    </div>`);
loader();

$(window).on(`load`, () => {
  $(`.loader-wrapper`).fadeOut('slow');
  $(`.hide`).each((i, elem) => {
    elem.classList.remove('hide');
  });
});

const PATH = location.pathname.replace(/^\/|\/$/g, '').split('/');
let scroll = 0;
// const token = mem.getToken()
const token = sessionStorage.getItem(`jwt`);

const renderPage = pageName => {
  if (token) {
    switch (pageName) {
      case 'admin':
        break;
      case 'account':
        renderAccount(token);
        break;
      case 'courses':
        renderVerifiedCourses(token);
        break;
      case 'course':
        renderCourse(token);
        break;
      case 'addCourse':
        renderAddCourse(token);
        break;
      case 'home':
        renderVerifiedHome(token);
        break;
      default:
        renderVerifiedHome;
    }
  } else {
    switch (pageName) {
      case 'admin':
        location.assign(`http://localhost:${PORT}/`);
        break;
      case 'account':
        location.assign(`http://localhost:${PORT}/`);
        break;
      case 'courses':
        renderCourses();
        break;
      case 'course':
        renderCourse();
        break;
      case 'addCourse':
        location.assign(`http://localhost:${PORT}/courses`);
        break;
      case 'home':
        renderHome();
        break;
      default:
        renderHome();
    }
  }
};

window.addEventListener('load', () => {
  switch (PATH[0]) {
    // Admin Page
    case `admin`:
      renderPage('admin');
      break;

    // Courses Page
    case `courses`:
      if (PATH[1]) {
        if (PATH[1] === `addCourse`) {
          renderPage('addCourse');
        } else {
          location.assign(`http://localhost:${PORT}/courses`);
        }
      } else {
        renderPage('courses');
      }
      break;

    case 'course':
      renderPage('course');
      break;

    case 'account':
      renderPage('account');
      break;

    // Home Page
    default:
      renderPage('home');
  }

  // Updates the footer to the current year.
  $(`#year`).html(new Date().getFullYear());

  // Setting the Profile Picture of the User
  let pic = $(`#profPic`);
  pic.on(`click`, e => {
    e.preventDefault();
    pic.toggleClass(`show`);
    $(`#account`).attr(`aria-expanded`, !$(`#account`).attr(`aria-expanded`));
    $(`div.dropdown-menu-right`).toggleClass(`show`);
  });

  $(`#profile`).on(`click`, e => {
    location.href = './account';
  });

  const renderForm = () => {
    switch (location.hash) {
      case `#register`:
        if (token) {
          location.hash = '';
          break;
        }
        renderRegister();
        document.documentElement.scrollTo(0, scroll);
        updateFormPos();
        break;
      case `#login`:
        if (token) {
          location.hash = '';
          location.reload();
          break;
        }
        renderLogin();
        document.documentElement.scrollTo(0, scroll);
        updateFormPos();
        break;
      case `#logout`:
        // mem.eraseToken()
        sessionStorage.removeItem(`jwt`);
        location.href = './';
        break;
    }
  };

  addEventListener('hashchange', renderForm);

  renderForm();
  addEventListener('resize', updateFormPos);
  addEventListener(`scroll`, updateFormPos);
});

// window.addEventListener(`load`, () => mem.checkAuth())
