export const getUserDetails = async token => {
  try {
    const response = await fetch(
      `https://course-booking-server.herokuapp.com/api/users/details`,
      {
        headers: {
          authorization: token,
        },
      }
    );

    let user = await response.json();
    return user;
  } catch (err) {
    console.error(err);
  }
};
