import $ from 'jquery';

let hide = false;

export const showForm = () => {
  if (hide) {
    clearTimeout(hide);
    hide = false;
  }
  $(`#form`).removeClass(`hide`);
  $(`#form`).addClass(`show-form`).removeClass(`hidden`);
  $(`.form-overlay`)
    .css({height: document.documentElement.scrollHeight})
    .on('click', closeForm);
  updateFormPos();
};

export const updateFormPos = () => {
  scroll = document.documentElement.scrollTop;
  let top =
    scroll +
    (window.innerHeight / 2 - $(`#form`).css(`height`).replace('px', '') / 2);
  let left = innerWidth / 2 - $(`#form`).css(`width`).replace('px', '') / 2;
  $(`#form`).css({top, left});
};

const x = () => {
  $(`.fa-times-circle`).on('click', closeForm);
};

const closeForm = () => {
  // $(`#form`).fadeOut(`slow`);
  $(`#form`).addClass(`hidden`);
  $(`.form-overlay`).css({height: '0px'});
  hide = setTimeout(() => {
    $(`#form`).addClass(`hide`);
  }, 1000);
  location.hash = ``;
};

export const processPass = () => {
  const eyes = document.querySelectorAll(`.fa-eye-slash`);
  const passInpts = document.querySelectorAll(`input[type='password']`);

  let showPass = false;

  eyes.forEach((eye, i) => {
    eye.addEventListener(`click`, e => {
      if (!showPass) {
        passInpts[i].setAttribute(`type`, `text`);
        eyes[i].style.color = '#333333';
        showPass = true;
      } else {
        passInpts[i].setAttribute(`type`, `password`);
        eyes[i].style.color = '#999999';
        showPass = false;
      }
    });
  });

  passInpts.forEach((inpt, i) => {
    inpt.addEventListener(`input`, e => {
      if (inpt.value && !showPass) {
        eyes[i].style.color = '#999999';
      } else if (!inpt.value) {
        eyes[i].style.color = '#FFFFFF';
      }
    });
  });
};

export const showReg = () => {
  $(`#form`).html(
    // <div class="col-md-6 offset-md-3">
    `<div class="form-title">
      <h2>Register</h2>
      <i class="far fa-times-circle"></i>
    </div>
    <form action="" id="registerUser" method="POST">
          <!-- First Name: -->
          <input
            type="text"
            name="firstName"
            id="firstName"
            class="form-control mb-3"
            placeholder="First Name"
            required
          />
          <!-- Last Name: -->
          <input
            type="text"
            name="lastName"
            id="lastName"
            class="form-control mb-3"
            placeholder="Last Name"
            required
          />
          <!-- Email: -->
          <input
            type="email"
            name="userEmail"
            id="userEmail"
            class="form-control mb-3"
            placeholder="Email Address"
            required
          />
          <!-- Mobile Number: -->
          <input
            type="tel"
            name="mobileNumber"
            id="mobileNumber"
            class="form-control mb-3"
            placeholder="Mobile Number"
            pattern="09[0-9]{9}"
            oninvalid="this.setCustomValidity('Input a valid PH number Ex. 09123456789 & +639123456789')"
            oninput="setCustomValidity('')"
          />

          <!-- Password Form -->
          <div class="form-row my-3">
            <!-- Insert Password -->
            <div class="form-group col-md-6 inptPass">
              <input
                type="password"
                name="password1"
                id="password1"
                class="form-control mb-3"
                placeholder="Input Password"
                required
              />
              <i class="fas fa-eye-slash"></i>
            </div>
            <!-- Verify Password -->
            <div class="form-group col-md-6 inptPass">
              <input
                type="password"
                name="password2"
                id="password2"
                class="form-control mb-3"
                placeholder="Verify Password"
                required
              />
              <i class="fas fa-eye-slash"></i>
            </div>
          </div>
          <button id="regSubmit" type="submit" class="btn btn-block btn-primary my-5">
            Sign Up
          </button>
          <p>Already have an account? <a href="#login">Sign In</a></p>
        </form>`
  );
  // </div>`);

  x();
};

export const showLogin = () => {
  $(`#form`).html(
    `<div class="form-title">
      <h2>Login</h2>
      <i class="far fa-times-circle"></i>
    </div>
    <div class="">
      <form action="" id="loginForm">
        <!-- Email: -->
        <input
          type="email"
          name="userEmail"
          id="userEmail"
          class="form-control mb-3"
          placeholder="Email Address"
          required
        />

        <!-- Password -->
        <div class="inptPass">
          <input
            type="password"
            name="password1"
            id="password1"
            class="form-control mb-3"
            placeholder="Password"
            required
          />
          <i class="fas fa-eye-slash"></i>
        </div>
        <button id="loginSubmit" type="submit" class="btn btn-block btn-primary my-5">
          Login
        </button>
        <p>Don't have an account? <a href="#register">Sign Up</></p>
      </form>
    </div>`
  );
  x();
};
