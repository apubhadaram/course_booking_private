import MiniCssExtractPlugin from 'mini-css-extract-plugin';
import CssMinimizerPlugin from 'css-minimizer-webpack-plugin';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import {merge} from 'webpack-merge';
import paths from './paths.js';
import common from './webpack.common.js';
import autoprefixer from 'autoprefixer';

export default merge(common, {
  mode: 'production',
  devtool: false,

  output: {
    path: paths.build,
    filename: './assets/js/[name].[contenthash].js',
    assetModuleFilename: 'static/images/[hash][ext][query]',
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node-modules/,
        use: ['babel-loader'],
      },

      // Video Separation
      {
        test: /\.(mp4|webm)$/i,
        type: 'asset/resource',
        generator: {
          filename: 'static/videos/[name].[hash].[ext]',
        },
      },

      // Image Separation
      {
        test: /\.(png|svg|jpg|jpeg|gif)$/i,
        type: 'asset/resource',
        generator: {
          filename: 'static/images/[name].[hash].[ext]',
        },
      },

      // Fonts Separation
      {
        test: /\.(woff(2)?|eot|ttf|otf|)$/i,
        type: 'asset/inline',
      },

      // Sass loader
      {
        test: /\.scss$/i,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          {
            loader: 'postcss-loader',
            options: {
              postcssOptions: {
                plugins: function () {
                  return [autoprefixer];
                },
              },
            },
          },
          'resolve-url-loader',
          {
            loader: 'sass-loader',
            options: {
              sourceMap: true,
            },
          },
        ],
      },
    ],
  },

  plugins: [
    new HtmlWebpackPlugin({
      filename: `index.html`,
      template: `./pages/index.html`,
      minify: {
        removeComments: true,
        collapseWhitespace: false,
        removeAttributeQuotes: true,
      },
    }),

    new HtmlWebpackPlugin({
      filename: `courses/addCourse/index.html`,
      template: `./pages/addCourse.html`,
      minify: false,
    }),

    new HtmlWebpackPlugin({
      filename: `courses/editCourse/index.html`,
      template: `./pages/editCourse.html`,
      minify: {
        removeComments: true,
        collapseWhitespace: false,
        removeAttributeQuotes: true,
      },
    }),

    new HtmlWebpackPlugin({
      filename: `courses/index.html`,
      template: `./pages/courses.html`,
      minify: {
        removeComments: true,
        collapseWhitespace: false,
        removeAttributeQuotes: true,
      },
    }),

    new HtmlWebpackPlugin({
      filename: `course/index.html`,
      template: `./pages/course.html`,
      minify: {
        removeComments: true,
        collapseWhitespace: false,
        removeAttributeQuotes: true,
      },
    }),

    new HtmlWebpackPlugin({
      filename: `account/index.html`,
      template: `./pages/account.html`,
      minify: {
        removeComments: true,
        collapseWhitespace: false,
        removeAttributeQuotes: true,
      },
    }),

    new MiniCssExtractPlugin({
      filename: './assets/css/[name].[contenthash].css',
    }),
  ],

  optimization: {
    minimize: true,
    minimizer: [new CssMinimizerPlugin(), '...'],
    // runtimeChunk: {
    //   name: 'runtime',
    // },
  },

  performance: {
    hints: false,
    maxEntrypointSize: 512000,
    maxAssetSize: 512000,
  },
});
