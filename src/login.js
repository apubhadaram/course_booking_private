import {authLog} from './auth.js';
import {showLogin, showForm, processPass} from './forms.js';
import $ from 'jquery';
// import mem from "./inMemoryJwt.js"

export default () => {
  showForm();
  showLogin();
  processPass();
  // const loginForm = document.querySelector(`#loginForm`);
  // loginForm.addEventListener('submit', authLog);
  $('#loginSubmit').on('click', e => {
    authLog(e);
    // location.reload();
  });
};
