import {getUserDetails} from './loggedIn.js';
import {configureNav} from './nav.js';
import moment from 'moment';
import $ from 'jquery';

export const renderAccount = async token => {
  configureNav(token);
  displayEnrollments(token);
};

const displayEnrollments = async token => {
  const user = await getUserDetails(token);
  console.log(typeof user.enrollments);
  console.log(user.enrollments);

  for (let enrollment of user.enrollments) {
    let courseId = enrollment.courseId;
    const course = await fetch(
      `https://course-booking-server.herokuapp.com/api/courses/?id=${courseId}`
    ).then(res => res.json());
    console.log(course);

    if (course.name == undefined) {
    } else {
      let desc = course.description.slice(0, 50);
      $(`#enrollments`).append(`
        <tr>
          <td>${course.name}</td>
          <td>${desc}...</td>
          <td>${moment(enrollment.enrolledOn).format('L')}</td>
          <td>${enrollment.status}</td>
        </tr>
      `);
    }
  }
};
